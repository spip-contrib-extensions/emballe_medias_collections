<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-collections?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'collections_description' => 'Criar coleções de mídias, de álbuns...',
	'collections_nom' => 'Coleções de mídias',
	'collections_slogan' => 'Criar coleções de mídias'
);
