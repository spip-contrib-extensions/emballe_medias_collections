<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-collections?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'collections_description' => 'Crear colecciones de medios, de álbumes...',
	'collections_nom' => 'Colecciones de medios',
	'collections_slogan' => 'Crear colecciones de medios'
);
