<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-collections?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'collections_description' => 'Maak mediacollecties, albums...',
	'collections_nom' => 'Mediacollecties',
	'collections_slogan' => 'Maak mediacollecties'
);
