<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/emballe_medias_collections.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'collections_description' => 'Créer des collections de médias, des albums...',
	'collections_nom' => 'Collections de médias',
	'collections_slogan' => 'Créer des collections de médias'
);
