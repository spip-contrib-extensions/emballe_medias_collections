<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-collections?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'collections_description' => 'Vytvorí kolekcie z multimédií, albumov, atď.',
	'collections_nom' => 'Kolekcie z multimédií',
	'collections_slogan' => 'Tvorte kolekcií z multimédií'
);
